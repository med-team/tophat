tophat (2.1.1+dfsg1-3) REMOVED; urgency=medium

  * Tophat was removed from Debian since it is consider obsolete by its
    authors (see bug #950947).
    As an additional hint there was a newer version than the last
    packaged one found here:
      https://github.com/DaehwanKimLab/tophat
    (thanks to Ben Tris for the hint)

  * Use 2to3 to port to Python3
    Closes: #938677
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/control
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Rely on pre-initialized dpkg-architecture variables.

 -- Andreas Tille <tille@debian.org>  Fri, 11 Oct 2019 09:54:16 +0200

tophat (2.1.1+dfsg1-2) unstable; urgency=medium

  * Fix Makefile.am
    Closes: #906510
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.0
  * Remove unneeded get-orig-source target

 -- Andreas Tille <tille@debian.org>  Sat, 18 Aug 2018 09:28:30 +0200

tophat (2.1.1+dfsg1-1) unstable; urgency=medium

  * Team upload

  * debian/upstream/metadata:
    - Added references to registries
    - yamllint cleanliness

  [ Nadiya Sitdykova ]
  * add info about debian/tests/test_data

  [ Fabian Klötzl ]
  * remove convenience copy of samtools.
    Closes: #780816
  * adapt tophat script to recent samtools version

  [ Andreas Tille ]
  * Remove unused license paragraph
  * Standards-Version: 4.1.1
  * Really remove samtools code copy also from source tarball
  * Versioned Depends: samtools (>= 1.5)
  * d/watch: enable numbered dfsg-appendices

 -- Fabian Klötzl <fabian@kloetzl.info>  Wed, 06 Dec 2017 16:13:31 +0100

tophat (2.1.1+dfsg-4) unstable; urgency=medium

  * Build-Depends: bowtie + Architecture: any
    Closes: #873866
  * Standards-Version: 4.1.0 (no changes needed)
  * Autoreconf is default with debhelper 10

 -- Andreas Tille <tille@debian.org>  Mon, 04 Sep 2017 13:36:19 +0200

tophat (2.1.1+dfsg-3) unstable; urgency=medium

  * Team upload.

  [ Nadiya Sitdykova ]
  * Add autopkgtest test-suite

  [ Andreas Tille ]
  * debhelper 10 (use --no-parallel to avoid build problems)
  * Standards-Version: 4.0.0 (no changes needed)
  * Drop unneeded gbp.conf
  * d/rules: use DEB_SOURCE

 -- Nadiya Sitdykova <rovenskasa@gmail.com>  Fri, 14 Jul 2017 21:15:50 -0400

tophat (2.1.1+dfsg-2) unstable; urgency=medium

  [ Sascha Steinbiss ]
  * Team upload.
  * Fix building with GCC 6.
    Closes: #811881.

  [ Andreas Tille ]
  * cme fix dpkg-control

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sat, 14 May 2016 15:53:10 +0000

tophat (2.1.1+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 2.1.1+dfsg
  * Refresh and delete unused patches
  * Apply cme fix dpkg
  * Export additional hardening flags - hardening=+all
  * Don't install 3rd party software (intervaltree,sortedcontainers)
  * Fix Makefile for the embedded copy of samtools-0.1.18
  * d/copyright:
    - Add Files: stanza for samtools-0.1.18
    - Add to Files-Excluded: intervaltree and sortedcontainers
    - Fix Source url

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 17 Mar 2016 14:22:58 +0100

tophat (2.1.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 18 Jul 2015 17:42:53 +0200

tophat (2.0.14+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 2.0.14+dfsg
  * d/patches: applied cme fix dpkg, s/Descriptions/Description/

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Wed, 29 Apr 2015 15:10:10 +0200

tophat (2.0.13+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * Adapted patches

  [ Alexandre Mestiashvili]
  * debian/patches/hardening4samtols.patch: add hardening flags for
    embedded copy of samtools

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Sun, 26 Oct 2014 15:53:02 +0100

tophat (2.0.12+dfsg-3) unstable; urgency=medium

  * Since tophat depends from bowtie|bowtie2 and now both versions
    are available only on amd64 tophat itself is also
     Architecture: amd64 kfreebsd-amd64
  * Do not "Suggests: bowtie" since it is just in "Depends"
  * cme fix dpkg-control
  * d/copyright: fix "empty-short-license-in-dep5-copyright" lintian
    warnings

 -- Andreas Tille <tille@debian.org>  Thu, 25 Sep 2014 08:08:34 +0200

tophat (2.0.12+dfsg-2) unstable; urgency=medium

  * d/rules:
     - Fix syntax of uscan call
     - Remove redundant enforcement of xz compression
  * d/control: Versioned Build-Depends: seqan-dev (>= 1.4)
    Closes: #754664

 -- Andreas Tille <tille@debian.org>  Fri, 22 Aug 2014 10:04:23 +0200

tophat (2.0.12+dfsg-1) unstable; urgency=medium

  [ Alexandre Mestiashvili ]
  * d/watch: new url
  * Imported Upstream version 2.0.12+dfsg
  * d/patches/fix_build_w_seqan1.4.patch: patch from Manuel Holtgrewe
    resolving #733352.
     removed const_ness_part1.patch as the new patch solves the problem
     completely
    Closes: #733352
  * debian/patches/fix_includes_path.patch: exclude SeqAn-1.3 from configure.ac

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 03 Jul 2014 21:06:10 +0200

tophat (2.0.11+dfsg-1) UNRELEASED; urgency=medium

  * New upstream version
  * debian/copyright: Update contact
  * Move debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control
  * debian/rules:
     - Make sure boost libraries will be found
     - some more invocations to help2man

 -- Andreas Tille <tille@debian.org>  Tue, 18 Mar 2014 13:54:41 +0100

tophat (2.0.10-1) unstable; urgency=low

  * Imported Upstream version 2.0.10

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 21 Nov 2013 09:57:21 +0100

tophat (2.0.9-1) unstable; urgency=low

  * d/get-orig-source: place tarballs to ../tarballs/
  * d/control: use canonical vcs fields, Standards-Version: 3.9.4
  * d/changelog: updated source field
  * refreshed patches
  * Imported Upstream version 2.0.9

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 04 Jul 2013 13:59:07 +0200

tophat (2.0.8b-1) unstable; urgency=low

  * Imported Upstream version 2.0.8b
    Closes: #707059

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Tue, 07 May 2013 22:58:36 +0200
tophat (2.0.8-1) unstable; urgency=low

  * Imported Upstream version 2.0.8

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Sat, 02 Mar 2013 12:38:32 +0100

tophat (2.0.7-1) unstable; urgency=low

  * Imported Upstream version 2.0.7

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Sat, 23 Feb 2013 11:45:36 +0100

tophat (2.0.6-1) unstable; urgency=low

  * Team upload

  [ Carlos Borroto ]
  * Imported Upstream version 2.0.6

 -- Charles Plessy <plessy@debian.org>  Tue, 06 Nov 2012 14:54:31 +0900

tophat (2.0.5-1) unstable; urgency=low

  [ Alexandre Mestiashvili ]
  * Imported Upstream version 2.0.5

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Fri, 05 Oct 2012 10:56:26 +0200

tophat (2.0.3-1) unstable; urgency=low

  * Imported Upstream version 2.0.3

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Sat, 09 Jun 2012 11:06:21 +0200

tophat (2.0.0-2) unstable; urgency=low

  * debian/upstream: BibTeX conform author syntax
  * debian/patces/bashism_in_shell_script.patch: Fix bashism in shell script
    Closes: #671813
  * debhelper 9 (control+compat)
  * debian/rules: Create some manpages using help2man

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2012 13:26:38 +0200

tophat (2.0.0-1) unstable; urgency=low

  [ Charles Plessy ]
  * YAML syntax correction

  [ Alexandre Mestiashvili ]
  * Imported Upstream version 2.0.0
  * updated debian/patches
  * debian/control added libboost-thread-dev dependency,
    added bowtie2 to Suggests:
  * debian/control  Standards-Version: 3.9.3 Depends: bowtie2
  * debian/copyright updated Format, removed seqan copyright statements
  * debian/patches removed unneeded patches
  * debian/get-orig-source - removes shipped with tophat SeqAn files.

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Sat, 14 Apr 2012 12:47:32 +0200

tophat (1.4.1-2) unstable; urgency=low

  [ Carlos Borroto ]
  * added bowtie as a binary dependency
  * suggests cufflinks

  [ Andreas Tille ]
  * debian/control: Added myself to uploaders
  * debian/patches/remove_cflag_handling_in_configure.patch: Enable better
    support for different architectures (Thanks to Peter Michael Green
    <plugwash@p10link.net> for the patch)
    Closes: #660309
  * debian/upstream: Added references

 -- Andreas Tille <tille@debian.org>  Sat, 18 Feb 2012 08:22:50 +0100

tophat (1.4.1-1) unstable; urgency=low

  * Team upload.
  * Initial upload to Debian archive.

  [ Carlos Borroto ]
  * New upstream release

  [ Charles Plessy ]
  * Corrected VCS URL.
  * Minor description changes.
  * Recommend bowtie.
  * debian/copyright:
    - Normalised with config-edit --application dpkg-copyright.
    - Documented SeqAn and m4 macros.
  * Merged patches bam2samtools and fix_includes_path.patch (same purpose).
  * Build-depend only on python.
  * Tighten build-dependancy on sequan-dev.

 -- Charles Plessy <plessy@debian.org>  Mon, 06 Feb 2012 16:12:29 +0900

tophat (1.4.0-1) UNRELEASED; urgency=low

  * New upstream release
    - debian/patches/fix_undefined_reference.patch

 -- Carlos Borroto <carlos.borroto@gmail.com>  Tue, 10 Jan 2012 11:49:39 -0500

tophat (1.3.3-1) UNRELEASED; urgency=low

  TODO:
  * src/SeqAn-1.2 should be excluded
    - tophat depends on seqan library which already exists in debian

  [Alexandre Mestiashvili]
  * New upstream release
  * Removed dh-make template from watch file
  * Added initial copyright data , removed templates , added Source
  * debian/compat version 8
  * debian/control
    - Maintainer: Debian Med Packaging Team
      <debian-med-packaging@lists.alioth.debian.org>
    - DM-Upload-Allowed: yes
    - Added myself to Uploaders:
    - added correct Vcs-Git , Vcs-Browser fields
    - Standards-Version: 3.9.2
    - added build-dependency quilt
    - added seqan-dev as dependency
  * debian/rules
    - removed dh-make template
  * debian/control Added python dependency to binary package
  * debian/control Description shouldn't start with package name
  * debian/rules removed quilt patch management
  * Added DEP3 headers to patches.
  * Added Pre-Depends: dpkg (>= 1.15.6) (xz compression) Fixed syntax-error in debian/copyright

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Thu, 05 Jan 2012 14:37:19 +0100

tophat (1.3.1-1ppa1~lucid1) lucid; urgency=low

  * New upstream version

 -- Carlos Borroto <carlos.borroto@gmail.com>  Thu, 23 Jun 2011 11:02:22 -0400

tophat (1.3.0-1ppa1~lucid1) lucid; urgency=low

  * Backported to lucid

 -- Carlos Borroto <carlos.borroto@gmail.com>  Wed, 08 Jun 2011 14:56:32 -0400

tophat (1.3.0-1) UNRELEASED; urgency=low

  * Initial release
  * debian/patches/fix_includes_path.patch
  * debian/patches/fix-configure.patch

 -- Carlos Borroto <carlos.borroto@gmail.com>  Wed, 08 Jun 2011 14:34:56 -0400
